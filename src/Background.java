import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class Background extends JPanel {
	
	public static int cellSize = 30;
	public static ArrayList<Integer> cells = new ArrayList<Integer>();
	public Image menu;
	public Image bush;
	public Image brick;
	public Image brick2;
	public Image block;
	public Image water;
	public Image eagle;
	public Tank tank;
	public static AI[] AItanks = new AI[2];
	public AI AItank;
	public AI AItank1;
	public static int AIspeed = 15;
	public static Battle_City main;
	public static int lives = 5;
	
	public static int points = 0;
	public static int opponentsLeft = 5;
	public static int currentLevel = 2;
	
	
	public Background(){
		tank = new Tank();
		for(int i=0;i<2;i++){
			AItanks[i] = new AI(new int[] {20,10+i*40},"Tank "+i);
		}
		bush = loadImage("Bush.png");
		block = loadImage("Block.png");
		brick = loadImage("Brick.png");
		brick2 = loadImage("Brick2.png");
		water = loadImage("Water.png");
		eagle = loadImage("Eagle.png");
		menu = loadImage("city.jpg");
		
		try {
			readCellsFromFile();
			updateDijkstraPaths();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		for(AI AItank:AItanks){
			AItank.shortestPathToCoordinates();
		}
		
	}
	
	public static void updateDijkstraPaths(){
		Dijkstra.obstacle_pixels2.clear();
		for(int i=1;i<cells.size()+1;i++){
			if(cells.get(i-1)!=0){
				Dijkstra.obstacle_pixels2.add(Integer.toString(i));
			}
		}
	}
	
	public void paintPath(Graphics g){
		int x=20;
		int y=20;
		for(int i : Dijkstra.shortestPath){
			if(i>18){
				x=20+((-1+i%18)*30);
				y=20+((i/18))*30;
				
			}
			else{
				x=20+(i-1)*30;
				y=20+((i/18))*30;
				
			}
			g.fillRect(x, y, 30, 30);
		}
	}
	
	public void paintLevel(final Graphics g2d){
		g2d.drawImage(menu, 0, 0, 600, 600, this);
		g2d.setFont(new Font("Arial", Font.BOLD, 40));
		g2d.drawString("Level: "+currentLevel, 230, 520);
		g2d.setFont(new Font("Arial", Font.BOLD, 20));
		g2d.drawString("Move: w,a,s,d,  Start: space, Shoot: k", 130,580);
//		Timer showLevelString = new Timer(10,new ActionListener(){
//
//			@Override
//			public void actionPerformed(ActionEvent arg0) {
//				g2d.setColor(new Color(0,0,0));
//				
//			}
//			
//		});
	}
	
	public void paint(Graphics g){
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON );  
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
		g2d.setColor(new Color(0,0,0));
		g2d.fillRect(0, 0, 580, 580);
		g2d.setFont(new Font("Arial", Font.BOLD, 15));
		g2d.drawString("Points: "+points, 20, 600);
		g2d.drawString("Opponents: "+opponentsLeft, 450, 600);
		g2d.drawString("Lives: "+lives, 250, 600);
		
		Tank.drawTank(g2d, this);
		for(AI AItank:AItanks){
			if(AItank.isAIMissileMoving){
				AItank.drawMissile(g, this);
			}
//			g.drawString(AItank.name, AItank.AItankCoordinates[0], AItank.AItankCoordinates[1]);
			AItank.drawAITank(g, this);
		}
		
		
		if(Tank.isMissileMoving){
			Tank.drawMissile(g, this);
		}
		drawCells(g2d);
		
		g.setColor(Color.gray);
		for(int i=1;i<18;i++){
			g.drawLine(20, 20+i*cellSize, 560, 20+i*cellSize);
			g.drawLine(20+i*cellSize, 20, 20+i*cellSize, 560);
		}
	
		if(!Battle_City.startGame){
			paintLevel(g2d);
		}
	}

	
	public static Image loadImage(String name){
		try {
			URL url = Battle_City.class.getResource("/images/" + name);
			return ImageIO.read(url);
		} catch (IOException e) {
			return null;
		}
	}
	
	
	public static void readCellsFromFile() throws FileNotFoundException{
		File level = null;
		try {
			level = new File(Battle_City.class.getClassLoader().getResource("levels/Level"+currentLevel).toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		Scanner sc = new Scanner(level);
		while(sc.hasNextLine()){
			String line[] = sc.nextLine().split(" ");
			for(int i=0;i<line.length;i++){
				cells.add(Integer.parseInt(line[i]));
			}
		}
		sc.close();
	}
	
	public static void resetLevel(){
		currentLevel++;
		if(currentLevel>3){
			JOptionPane.showMessageDialog(null, "Nice job lad", "Yerr a tank",
                    JOptionPane.PLAIN_MESSAGE);
			System.exit(0);
		}
		
		cells.clear();
		try {
			readCellsFromFile();
			updateDijkstraPaths();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		AIspeed=15;
		
		int i=0;
		for(AI AItank:AItanks){
			AItank.AItankCoordinates = new int[] {20,10+i*40};
			AItank.tankTimer.setDelay(AIspeed);
			AItank.pathCoordinates.clear();
			AItank.shortestPathToCoordinates();
			i++;
		}
		opponentsLeft=currentLevel*5;
		Tank.tankCoordinates = new int[] {320,320};
	}
	
	
	
	public static boolean objectCollidesWithCell(int objectX, int objectY,int size){
		int x = 20;
		int y = 20;
		boolean isMissile = (size==7);
		Rectangle object = new Rectangle(objectX, objectY, size, size);
		
		if(isMissile && object.intersects(new Rectangle(Tank.tankCoordinates[0],Tank.tankCoordinates[1],30,30))){
			Tank.tank = loadImage("");
			lives--;
			if(lives==0){
				JOptionPane.showMessageDialog(null, "Ye fucked up", "Notification of a fuck-up",
                        JOptionPane.PLAIN_MESSAGE);
				System.exit(0);
			}
			return true;
		}
		
		for(AI AItank:AItanks){
		
			if(!isMissile && object.intersects(new Rectangle(AItank.AItankCoordinates[0],AItank.AItankCoordinates[1],30,30))){
				return true;
			}
			if(isMissile && object.intersects(new Rectangle(AItank.AItankCoordinates[0],AItank.AItankCoordinates[1],30,30)) && Tank.isMissileMoving){
				points+=100;
				opponentsLeft--;
				if(opponentsLeft!=0){
					AItank.AItankCoordinates[0]=140;
					AItank.AItankCoordinates[1]=70;
					AItank.pathCoordinates.clear();
					AItank.shortestPathToCoordinates();
					if(AIspeed>3)
						AIspeed-=1;
					AItank.tankTimer.setDelay(AIspeed);
				}
				else{
					Battle_City.startGame=false;
					for(AI AItankss:AItanks){
						AItankss.tankTimer.stop();
					}
					resetLevel();
				}
				
				return true;
			}
		}


		for(int i=1;i<cells.size()+1;i++){
			boolean isEmptyCell = cells.get(i-1)==0;
			boolean isBushCell = cells.get(i-1)==2;
			if(!isEmptyCell && !isBushCell  && object.intersects(new Rectangle(x,y,30,30))){
				if(isMissile){
					switch(cells.get(i-1)){
						case 1:
							cells.set(i-1, 5);
							break;
						case 5:
							cells.set(i-1, 0);
							Dijkstra.obstacle_pixels2.remove(""+i);
							break;
					}
					
				}
				return true;
			}
			x+=30;
			if(i%18==0){
				y+=30;
				x=20;
			}
		}
	
		return false;
	}
	
	public void drawCells(Graphics g){
		int x = 20;
		int y = 20;
		for(int i=1;i<cells.size()+1;i++){
			switch(cells.get(i-1)){
				case 1:
					g.drawImage(brick, x, y, 30, 30, this);
					break;
				case 2:
					g.drawImage(bush, x, y, 30, 30, this);
					break;
				case 3:
					g.drawImage(block, x, y, 30, 30, this);
					break;
				case 4:
					g.drawImage(water, x, y, 30, 30, this);
					break;
				case 5:
					g.drawImage(brick2, x, y, 30, 30, this);
					break;
				case 6:
					g.drawImage(eagle, x, y, 30, 30, this);
					break;
			}
			x+=30;
			
			if(i%18==0){
				y+=30;
				x=20;
			}
			
		}
	}
	
	

}
