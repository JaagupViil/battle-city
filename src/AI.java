import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;


public class AI extends JPanel {

	
	private static final long serialVersionUID = 1L;
//	public static int[][] AItankCoordinates = {{20,20},{20,20},{20,20},{20,20}}; //X, Y
	public int[] AItankCoordinates; //X, Y
	public int[] missileCoordinates = {0,0};
	public Image AItank;
	public ArrayList<Integer[]> pathCoordinates = new ArrayList<Integer[]>();
	
	
	
	public Timer missileTimer;
	public Timer tankTimer;
	public int missileMoveCounter = 0;
	public boolean isAIMissileMoving = false;
	public char lastMoveDirection;
	public Dijkstra algorithm = new Dijkstra("1","323");
	public String name;
	
	public AI(int[] coordinates, String name){
		
		AItank = Toolkit.getDefaultToolkit().getImage(new java.io.File("").getAbsolutePath()+"/src/images/AIS.png");
		AItankCoordinates = coordinates;
		this.name = name;
		this.repaint();
		
		
	}
	
	
	public void drawAITank(Graphics g, ImageObserver ob ){
		g.drawImage(AItank, AItankCoordinates[0], AItankCoordinates[1], 30, 30, ob);
	}
	
	public void drawMissile(Graphics g, ImageObserver ob){
		g.setColor(Color.gray);
		g.fillRect(missileCoordinates[0], missileCoordinates[1], 7, 7);
	}
	
	public int returnCellNumber(int[] coordinates){
		int row = 1+((coordinates[1]-20)/30);
		int column = 1+((coordinates[0]-20)/30);
		
		return (row*18-(18-column));
		
	}
	
	public void shortestPathToCoordinates(){
    	algorithm.setNodes(""+returnCellNumber(AItankCoordinates), ""+returnCellNumber(Tank.tankCoordinates));
    	algorithm.initialize();
    	for (int cellNumbers:algorithm.shortestPath){
    		int x=0;
    		int y=0;
    		if(cellNumbers%18!=0){
    			x=((cellNumbers%18)*30);
    			y=(cellNumbers/18)*30;
    		}
    		else{
    			x=18*30;
    			y=(cellNumbers/18)*30;
    		}
    		Integer[] coordinate = {x,y};
    		pathCoordinates.add(coordinate);
    	}
    	algorithm.clear();
	}
	
	
	public int randomInteger(int range){
		Random generator = new Random();
		int random = generator.nextInt(range)+1;
//		System.out.println(random);
		return random;
	}
	
	public void moveTank(){
		tankTimer = new Timer(10,new ActionListener(){
			int x = AItankCoordinates[0];
			int y = AItankCoordinates[1];
			int moveCounter = 0;
			public void actionPerformed(ActionEvent arg0) {
				if(pathCoordinates.size()!=0){
					if(AItankCoordinates[0]+10<pathCoordinates.get(0)[0]){
						AItankCoordinates[0]+=1;
						AItank = Background.loadImage("AID.png");
						lastMoveDirection='d';
					}
					else if(AItankCoordinates[1]-20<pathCoordinates.get(0)[1]){
						AItankCoordinates[1]+=1;
						AItank = Background.loadImage("AIS.png");
						lastMoveDirection='s';
					}
					else if(AItankCoordinates[0]+10>pathCoordinates.get(0)[0]){
						AItankCoordinates[0]-=1;
						AItank = Background.loadImage("AIA.png");
						lastMoveDirection='a';
					}
					else if(AItankCoordinates[1]-20>pathCoordinates.get(0)[1]){
						AItankCoordinates[1]-=1;
						AItank = Background.loadImage("AIW.png");
						lastMoveDirection='w';
					}
					
					else{
						if(moveCounter>=randomInteger(300)){
							pathCoordinates.clear();
							shortestPathToCoordinates();
							moveCounter=0;
							if(!isAIMissileMoving)
								shoot();
						}
						else{
							pathCoordinates.remove(0);
						}
					}
				}
				if(moveCounter>=400){
					pathCoordinates.clear();
					shortestPathToCoordinates();
					moveCounter=0;
				}
				moveCounter++;
				Battle_City.frame.repaint();
			}
			
			
		});
		
		tankTimer.start();
	}
	
	public void shoot() {
		missileCoordinates[0] = AItankCoordinates[0]+13;
		missileCoordinates[1] = AItankCoordinates[1]+13;
		final char key = lastMoveDirection;
		missileTimer = new Timer(10,new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				missileMoveCounter+=1;
				
				isAIMissileMoving=true;
				
				switch( key ) { 
			        case 'w':
			        	missileCoordinates[1]+=-5;
			            break;
			        case 'a':
			        	missileCoordinates[0]+=-5;
			            break;
			        case 's':
			        	missileCoordinates[1]+=5;
			            break;
			        case 'd':
			        	missileCoordinates[0]+=5;
			            break;
				}
				
				Battle_City.frame.repaint();
				
				if(missileMoveCounter>5){
					if(Background.objectCollidesWithCell(missileCoordinates[0], missileCoordinates[1],7)){
						missileTimer.stop();
						missileMoveCounter=0;
						isAIMissileMoving=false;
					}
				}
			
				
				
				
				if(missileMoveCounter>=100){
					missileTimer.stop();
					missileMoveCounter=0;
					isAIMissileMoving=false;
				}
			}
		});
		missileTimer.start();
	}
	
	
	
	
	
	
}
