import java.util.ArrayList;
/**
 * 
 * 
 * An old implementation of the dijkstra algorithm.
 * Pappardelle with meatballs.
 * 
 * @author Jaks
 *
 */

public class Dijkstra {
	
	public int columns = 18;
	public static ArrayList<String> obstacle_pixels = new ArrayList<String>();
	public static ArrayList<String> obstacle_pixels2 = new ArrayList<String>();
	public static ArrayList<String> startendNode = new ArrayList<String>();
	public static ArrayList<String> path = new ArrayList<String>();
	public String[] nodes= new String[(columns*columns)];
	public ArrayList<String> node_weights = new ArrayList<String>();
	public int[] weights = new int[nodes.length];
	
	public  ArrayList<String> paths = new ArrayList<String>();
	public  static ArrayList<Integer> shortestPath = new ArrayList<Integer>();
//	public  static ArrayList<String> shortestPath = new ArrayList<String>();
	public static String startNode;
	public static String node;
	public boolean found=false;
//	
	public Dijkstra(String startNode,String node){
		this.startNode=startNode;
		this.node=node;
	}
	
	/**
	 * Set start node and end node to calculate the shortest path.
	 * 
	 * @param startNode
	 * @param endNode
	 */
	
	public void setNodes(String startNode,String endNode){
		this.startNode=startNode;
		this.node=endNode;
	}
	public int getN(String a){
		int j = 0;
		for(int i = 0;i<nodes.length;i++){
			if(nodes[i].equals(a)){
//				System.out.println(nodes[i]+" "+a);
				j=i;
			}
		}
		return j;
	}
	

	
	public void updatePaths(int min,String currentNode,boolean[] visited){
		int bN = Integer.parseInt(currentNode)-1;
		int nN = Integer.parseInt(currentNode)+1;
		int dN = Integer.parseInt(currentNode)-columns;
		int uN = Integer.parseInt(currentNode)+columns;
//		if(dN<0){
//			dN=0;
//		}
		if(uN>=(columns*columns)){
			uN=uN-columns;
		}
		if(bN<0 || bN%columns==0){
			bN=bN+1;
		}
		if(nN>=(columns*columns)){
			nN=nN-1;
		}
		if((nN-1)%(columns*columns)==0){
			nN=nN-1;
		}
		
		int[] t = {dN,uN,bN,nN};
		
//		System.out.println("Currentnode on "+currentNode+" ja teised "+dN+" "+uN+" "+bN+" "+nN);
		for(int i = 0;i<t.length;i++){
				if(node_weights.contains(currentNode+"-"+t[i]+":1") || node_weights.contains(t[i]+"-"+currentNode+":1")){ //A-B or B-A
					if(visited[t[i]-1]==false){
						if(1+min<weights[t[i]-1]){
//							System.out.println(currentNode+"-"+t[i]);
							paths.add(currentNode+"-"+t[i]+">"+min+";"+1);
							weights[t[i]-1]=1+min;
							int row = 1+(Integer.parseInt(currentNode)/100);
							if(Integer.parseInt(currentNode)%100==0){
								row=row-1;
							}
							int column = Integer.parseInt(currentNode)+100-100*row;
							
							
							path.add(row+":"+column);
							if((""+t[i]).equals(node)){
//								System.out.println("Path found");
								found=true;
								break;
							}
							
						}
					}
				}
		}
	}
	

	public void findPath(String startNode,String endNode){
//		System.out.println("Finding path");
		boolean[] visited = new boolean[nodes.length];
		
		for(int i=0;i<nodes.length;i++){
			weights[i]=Integer.MAX_VALUE;
			visited[i]=false;
			if(nodes[i].equals(startNode)){
				visited[i]=true;
			}
		}
//		System.out.println(Arrays.toString(visited));
//		System.out.println(Arrays.toString(weights));
		
		updatePaths(0,startNode,visited);
		for(int i = 0;i<visited.length;i++){
//				System.out.println(nodes[i]);
				if(!found){
//					System.out.println("Mitu korda "+i);
					updatePaths(weights[getNonMinVisitedNode(visited)],nodes[getNonMinVisitedNode(visited)],visited);
					visited[getNonMinVisitedNode(visited)]=true;
				}
		}
		
//		System.out.println(paths);
		shortestPath.clear();
		printPath(0,startNode,endNode);
	}
	
	public void printPath(int totalweight,String startNode,String endNode){
		int min = 100000;
		String fNode ="";
		String lNode = "";
		
		for(int i = 0;i<paths.size();i++){
			String lastNode = paths.get(i).split(">")[0].split("-")[1];
			int lastWeight = Integer.parseInt(paths.get(i).split(">")[1].split(";")[1]);
			if(lastNode.equals(endNode) && lastWeight<min){ //vb peab liitma e-f
				fNode = paths.get(i).split(">")[0].split("-")[0];
				lNode = paths.get(i).split(">")[0].split("-")[1];
				min=lastWeight;
			}
			
		}
//			System.out.println(fNode+"-"+lNode);
		if(lNode!="")
			shortestPath.add(0,Integer.parseInt(lNode));
//		shortestPath.add(0,fNode+"-"+lNode);
		
		try{
			int row = 1+(Integer.parseInt(lNode)/columns);
			if(Integer.parseInt(lNode)%columns==0){
				row=row-1;
			}
			int column = Integer.parseInt(lNode)+columns-columns*row;
			
			
//			bb.path.add(row+":"+column);
			if(fNode.equals(startNode)){
				totalweight=totalweight+min;
//				System.out.print(shortestPath+" ");
//				System.out.println(totalweight);
				
			}
			else{
				printPath(totalweight+min,startNode,fNode);
			}
		}
		catch(Exception e){
//			System.out.println("No path found");
		}
		
		
	}
	
	public int getNonMinVisitedNode(boolean[] visited){
		int min = Integer.MAX_VALUE;
		int node_nr = 0;
		for(int i = 0;i<visited.length;i++){
//			System.out.println(i);
			if(visited[i]==false && weights[i]<min){    
					min=weights[i];
					node_nr=i;
			}
		}
		return node_nr;
	}
	
	public void initialize() {
//		populateOb();

		for(int i=0;i<nodes.length;i++){
			nodes[i]=""+(i+1);
		}
		
		for(int i=0;i<nodes.length-1;i++){

			String fNode = ""+(i+1);
			String sNode = ""+(i+2);
			String dNode = ""+(i+(columns+1));
			if((i+1)%columns!=0){
				if(!obstacle_pixels2.contains(fNode) && !obstacle_pixels2.contains(sNode)){
					node_weights.add(fNode+"-"+sNode+":1");
//					System.out.println(fNode+"-"+sNode);
				}
			}
			if(!obstacle_pixels2.contains(dNode) && !obstacle_pixels2.contains(fNode)){
				node_weights.add(fNode+"-"+dNode+":1");
			}
		
		}
	
		
//		System.out.println(b.obstacle_pixels2);
//		System.out.println(obstacle_pixels2);
//		System.out.println(node_weights);
		long sT = System.currentTimeMillis();
		findPath(startNode,node); //uuenda seost mitte currentNode=node vaid nodes[i]=node
//		System.out.println(shortestPath);
		long t = System.currentTimeMillis()-sT;
//		System.out.println(t/1000);
//		System.out.println(nodes[12]); 
		clear();
//		

	}
	public void clear(){
//		System.out.println("clearing");
		paths.clear();
		node_weights.clear();
		weights = new int[nodes.length];
		nodes= new String[(columns*columns)];
		path.clear();
		found=false;
//		shortestPath.clear();
	}
	public void populateOb(){
		for(int i=0;i<obstacle_pixels.size();i++){
			int x = Integer.parseInt(obstacle_pixels.get(i).split(":")[0]);
			int y = Integer.parseInt(obstacle_pixels.get(i).split(":")[1]);
			int z = columns*x-columns+y;
			obstacle_pixels2.add(""+z);
		}
	}
}
