import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;

import javax.swing.JPanel;
import javax.swing.Timer;


public class Tank extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int[] tankCoordinates = {320,320}; //X, Y
	public static int[] missileCoordinates = {0,0};
	public static Image tank;
	
	public static Timer missileTimer;
	public static Timer tankTimer;
	public static int tankMoveCounter = 0;
	static int missileMoveCounter = 0;
	public static boolean isMissileMoving = false;
	public static char lastMoveDirection;
	
	public Tank(){
		tank = Toolkit.getDefaultToolkit().getImage(new java.io.File("").getAbsolutePath()+"/src/images/TankD.png");
		this.repaint();
		
	}
	
	public static void drawTank(Graphics g, ImageObserver ob ){
		g.drawImage(tank, tankCoordinates[0], tankCoordinates[1], 30, 30, ob);
		g.setColor(Color.red);
		
	}
	
	public static void drawMissile(Graphics g, ImageObserver ob){
		g.setColor(Color.gray);
		g.fillRect(missileCoordinates[0], missileCoordinates[1], 7, 7);
	}
	
	

	public static void changeTankCoordinates(final int axis, final int moveDistance, char keyChar){
		tankCoordinates[axis]+=moveDistance/6;
		if (Background.objectCollidesWithCell(tankCoordinates[0], tankCoordinates[1],30)){
			tankCoordinates[axis]-=moveDistance/6;
		}
		boolean xOutOfBorders = tankCoordinates[0]>530 || tankCoordinates[0]<20; 
		boolean yOutOfBorders = tankCoordinates[1]>530 || tankCoordinates[1]<20; 
		if(xOutOfBorders || yOutOfBorders){
			tankCoordinates[axis]-=moveDistance/6;
		}
			lastMoveDirection = keyChar;
			tank = Background.loadImage("Tank"+Character.toUpperCase(lastMoveDirection)+".png");
	}
	
	public void moveTank(final int axis, final int moveDistance, final char keyChar){
		tankTimer = new Timer(30,new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				tankCoordinates[axis]+=1;
				tankMoveCounter++;
				
				if (Background.objectCollidesWithCell(tankCoordinates[0], tankCoordinates[1],30)){
					tankCoordinates[axis]-=1;
				}
				boolean xOutOfBorders = tankCoordinates[0]>530 || tankCoordinates[0]<20; 
				boolean yOutOfBorders = tankCoordinates[1]>530 || tankCoordinates[1]<20; 
				if(xOutOfBorders || yOutOfBorders){
					tankCoordinates[axis]-=1;
				}
				
				lastMoveDirection = keyChar;
				tank = Background.loadImage("Tank"+keyChar+".png");
				Background.main.frame.repaint();
				if(tankMoveCounter==5){
					tankTimer.stop();
				}
				
			}
		});
		tankTimer.start();
	}

	public static void shoot() {
		missileCoordinates[0] = tankCoordinates[0]+13;
		missileCoordinates[1] = tankCoordinates[1]+13;
		final char key = lastMoveDirection;
		missileTimer = new Timer(10,new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				missileMoveCounter+=1;
				
				isMissileMoving=true;
				
				switch( key ) { 
			        case 'w':
			        	missileCoordinates[1]+=-5;
			            break;
			        case 'a':
			        	missileCoordinates[0]+=-5;
			            break;
			        case 's':
			        	missileCoordinates[1]+=5;
			            break;
			        case 'd':
			        	missileCoordinates[0]+=5;
			            break;
				}
				
				Battle_City.frame.repaint();
				
				if(missileMoveCounter>5){
					if(Background.objectCollidesWithCell(missileCoordinates[0], missileCoordinates[1],7)){
						missileTimer.stop();
						missileMoveCounter=0;
						isMissileMoving=false;
					}
				}
				
				if(missileMoveCounter>=100){
					missileTimer.stop();
					missileMoveCounter=0;
					isMissileMoving=false;
				}
			}
		});
		missileTimer.start();
	}

}
