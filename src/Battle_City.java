import java.awt.Color;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;


public class Battle_City {

	public static JFrame frame;
	public static boolean startGame = false;

	
	public static void main(String[] args) {
		final Background backGround = new Background();
		frame = new JFrame("Battle City"); 	
		frame.setSize(585,640);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.add(backGround);
		
		frame.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				char keyChar=e.getKeyChar();
			    switch( keyChar ) { 
			    
			        case 'w':
			        	Tank.changeTankCoordinates(1,-30,keyChar);
			            break;
			        case 'a':
			        	Tank.changeTankCoordinates(0,-30,keyChar);
			            break;
			        case 's':
			        	Tank.changeTankCoordinates(1,30,keyChar);
			            break;
			        case 'd':
			        	Tank.changeTankCoordinates(0,30,keyChar);
			            break;
			        case 'k':
			        	if (!Tank.isMissileMoving)
			        		Tank.shoot();
			        	break;
			        case ' ':
			        	
			        	if(!startGame){
			        		startGame=true;
			        		for(AI AItank:backGround.AItanks){
				        		AItank.moveTank();
				        	}
			        	}
			        	
			        	break;
			     }
			    frame.repaint();
				
			}
		});
		frame.addMouseMotionListener(new MouseMotionListener(){

			@Override
			public void mouseDragged(MouseEvent arg0) {
			}

			@Override
			public void mouseMoved(MouseEvent arg0) {
				Point a = new Point(frame.getMousePosition());
				int row = 1+((a.y-45)/30);
				int column = 1+((a.x-20)/30);
//				System.out.println(a.x-20+" "+(a.y-45));
//				System.out.println(row+" "+column);
			}
			
			
		});
		
	}
}
